
This is a convenient program for recording screens in GIF format, which takes up little space and is easy to use.

<h2 align="center">How the program works</h2>

### 1.How to use the program

After starting the program, you can press the key combination "Shift + Win + C", select the zone (by holding down the left mouse button (if you unsuccessfully select the zone, then you need to repeat the "Shift + Win + C" key combination)) and start recording by pressing "F2", if you want to cancel the selected area press the key combination "Win + Esc". To stop recording, press the key combination "Win + Esc" (to stop recording and close the selection area) or "F2" (to stop recording and save the selected area), then a notification will appear stating that recording is complete. The captured GIF is located in the program folder -> GIF. To close at all, you need to press the key combination "Shift + Win + V".


### 2.Options

In order to open the settings window, you need to press the key combination "Shift + Win + O", after which you can adjust: the maximum number of frames, the delay between frames, the compression ratio of the resolution, as well as adjust the cursor (you can remove it so as not to interfere, or change its appearance by linking to a new one).



### 3.Program installation

Run the Setup file (Setup.vbs) located in the "installation" folder. Then install the ImageMagick along the path "C:\Program Files". It is important to select all installation options for the program during installation:



After installing ImageMagick, you can also use it as a GIF editor.

Then the installation window of the program itself appears, complete the installation. Before using the program, set the screen scale to 100%.

### 4.Installing autorun

Hold down the "Win" button and press the "R" button, enter "shell:startup" in the field that appears, then press "Enter". Then move there the file named Video-recording (Video-recording.vbs), which is located in the "installation" folder. The program will now start automatically.

#### Step by step in photos

1. Enter shell:startup into the run window


2. Move Video-recording (Video-recording.vbs) to the appeared startup folder


### 5.Benefits of the program over others



<p align="center">My program</p>

Also important is the simple, intuitive recording control with the ability to edit GIFs in ImageMagick. The program is easy to manage and install. Enjoy your use.
